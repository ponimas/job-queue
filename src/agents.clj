(ns agents
  (:require
   [mount.core :refer [defstate]]
   [clojure.core.async :as a]
   [config :refer [config]]))

(defstate agents
  :start  {:agents (atom {})
           :output (a/chan (:output-buf-sz config))}
  :stop nil)

(defn new-agent! [{:keys [id] :as agent}]
  (swap! (:agents agents) #(assoc % id agent)))

(defn assign! [agent job]
  (a/put!  (:output agents)
           {:job_assigned {:job_id (:id job) :agent_id (:id agent)}}))

(defn by-id [agent-id]
  (get @(:agents agents)  agent-id))

(defn output! []
  (loop [res []]
    (if-let [x (a/poll! (:output agents))]
      (recur (conj res x))
      res)))

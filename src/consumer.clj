(ns consumer
  (:require
   [clojure.tools.logging :as log]
   jobs agents schemas))

(defmulti ^:private -consume (comp first keys))

(defmethod -consume :new_job [{:keys [new_job]}]
  (jobs/enqueue! new_job))

(defmethod -consume :new_agent [{:keys [new_agent]}]
  (agents/new-agent! new_agent))

(defmethod -consume :job_request [{{agent_id :agent_id} :job_request}]
  (if-let [agent (agents/by-id agent_id)]
    (when-let [job (jobs/get-job! agent)]
      (agents/assign! agent job))
    (log/error "no such agent" agent_id)))

(defmethod -consume :default [msg]
  (log/warn "unknown message" msg))

(defn consume! [msg]
  (some-> msg
          schemas/msg-coerce
          -consume))

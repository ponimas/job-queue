(ns core
  (:require
   [cheshire.core :as json]
   [mount.core :as mount]
   consumer agents))

(defn process [messages]
  (doseq [msg messages]
    (consumer/consume! msg)))

(defn -main []
  (mount/start)
  (-> (slurp *in*)
      (json/decode true)
      process)

  (println
   (json/generate-string (agents/output!) {:pretty true})))

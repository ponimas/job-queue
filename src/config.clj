(ns config
  (:require [mount.core :refer [defstate]]
            [clojure.edn :as edn]
            [clojure.tools.logging :as log]))

(def default-config
  {:skillset      #{"rewards-question" "bills-questions"}
   :jobs-buf-sz   1024
   :output-buf-sz 1024})

(def config-file "resources/config.edn")

(defn ->config [file]
  (try
    (merge default-config (edn/read-string (slurp file)))
    (catch Exception ex
      (log/error ex "Error while loading config file. Will use defaults.")
      default-config)))

(defstate config
  :start (->config config-file)
  :stop nil)

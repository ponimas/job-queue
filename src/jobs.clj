(ns jobs
  (:require
   [clojure.core.async :as a]
   [config :refer [config]]
   [mount.core :refer [defstate]]))

(defn ->jobs [config]
  (into {} (for [skill (:skillset config)]
             [skill {:urgent     (a/chan (:jobs-buf-sz config))
                     :uncritical (a/chan (:jobs-buf-sz config))}])))

(defstate jobs
  :start  (->jobs config)
  :stop nil)

(defn enqueue! [{:keys [type urgent] :as job}]
  (let [urgency (if urgent :urgent :uncritical)]
    (a/put! (get-in jobs [type urgency]) job)))

(defn- skills-chans
  "Returns list of channels for skills list"
  [skills]
  (let [urgent (for [skill skills] (get-in jobs [skill :urgent]))
        minor  (for [skill skills] (get-in jobs [skill :uncritical]))]
    (concat urgent minor)))

(defn get-job! [{:keys [primary_skillset secondary_skillset]}]
  (let [primary    (skills-chans primary_skillset)
        secondary  (skills-chans secondary_skillset)
        [job chan] (a/alts!! (concat primary secondary) :default nil :priority true)]
    (when-not (= chan :default)
      job)))

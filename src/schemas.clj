(ns schemas
  (:require
   [schema.coerce :as coerce]
   [schema.core :as s]
   [schema.utils :as sutils]
   [clojure.tools.logging :as log]
   [mount.core :refer [defstate]]
   [config :refer [config]]))

(defn skills-in-list [skills]
  (every? #(contains? (:skillset config) %) skills))

(def skillset
  (s/constrained [s/Str] skills-in-list "unknown skill"))

(def skill
  (s/constrained s/Str  #(contains? (:skillset config) %) "unknown skill"))

(def primary-skillset
  (s/constrained skillset not-empty "primary skillset can't be empty"))

(def new-agent
  {:id                 s/Uuid
   :name               s/Str
   :primary_skillset   primary-skillset
   :secondary_skillset skillset})

(def new-job
  {:id     s/Uuid
   :type   skill
   :urgent s/Bool})

(def job-request
  {:agent_id s/Uuid})

(def messages
  (s/conditional
   :new_agent {:new_agent new-agent}
   :new_job {:new_job new-job}
   :job_request {:job_request job-request}))

(def msg-coercer
  (coerce/coercer messages coerce/json-coercion-matcher))

(defn msg-coerce [msg]
  (let [coerced (msg-coercer msg)]
    (if (sutils/error? coerced)
      (log/error coerced)
      coerced)))

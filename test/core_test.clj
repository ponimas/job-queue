(ns core-test
  (:require
   [cheshire.core :as json]
   [clojure.test :refer :all]
   [mount.core :as mount]
   core agents))

(deftest integration
  (mount/start)
  (let [[input output]
        (mapv
         #(-> % slurp (json/decode true))
         ["provided/sample-input.json.txt" "provided/sample-output.json.txt"])]
    (core/process input)
    (is (= (json/encode output) (json/encode (agents/output!)))))
  (mount/stop))

(ns consumer-test
  (:require
   [clojure.test :refer :all]
   [mount.core :as mount]
   consumer jobs)
  (:refer-clojure :exclude [agent]))

(def test-cfg
  {:skillset      #{"1" "2" "3" "4"}
   :jobs-buf-sz   1024
   :output-buf-sz 1024})

(use-fixtures :each
  (fn [f]
    (mount/start-with {#'config/config test-cfg})
    (f)
    (mount/stop)))

(deftest invalid-msgs
  (testing "should ignore garbage messages"
    ;; look ma no exceptions
    (consumer/consume! {})
    (testing "no such skill in skillset"
      (consumer/consume!
       {:new_job {:id     "f26e890b-df8e-422e-a39c-7762aa0bac36"
                  :type   "rewards-question"
                  :urgent false}}))
    (testing "missing required key"
      (consumer/consume!
       {:new_job {:type "1" :urgent false}}))))


(def first-id "f26e890b-df8e-422e-a39c-7762aa0bac36")
(def second-id "a00e890b-df8e-422e-a39c-7762aa0bac36")
(def third-id "a00e890b-df8e-422e-a39c-7762aa0b0000")

(def agent
  {:id                 "ed0e23ef-6c2b-430c-9b90-cd4f1ff74c88"
   :name               "skilled one",
   :primary_skillset   [],
   :secondary_skillset []})

(def jobs
  [{:new_job
    {:id     first-id
     :type   "1"
     :urgent false}}
   {:new_job
    {:id     second-id
     :type   "2"
     :urgent true}}
   {:new_job
    {:id     third-id
     :type   "3"
     :urgent false}}])


(deftest shouldnt-get-without-skill
  (doseq [m jobs] (consumer/consume! m))
  (let [agent* (assoc agent
                      :primary_skillset ["1" "3"]
                      :secondary_skillset [])]

    (consumer/consume! {:new_agent agent*})

    (is (= first-id (str (:id (jobs/get-job! agent*)))))
    (is (= third-id (str (:id (jobs/get-job! agent*)))))
    (is (nil? (jobs/get-job! agent*)))))


(deftest should-get-secondary-last
  (doseq [m jobs] (consumer/consume! m))
  (let [agent* (assoc agent
                      :primary_skillset ["1"]
                      :secondary_skillset ["3" "2"])]

    (consumer/consume! {:new_agent agent*})

    (is (= first-id (str (:id (jobs/get-job! agent*)))))
    (is (= second-id (str (:id (jobs/get-job! agent*)))))
    (is (= third-id (str (:id (jobs/get-job! agent*)))))
    (is (nil? (jobs/get-job! agent*)))))


(deftest should-get-urgent-first
  (doseq [m jobs] (consumer/consume! m))
  (let [agent* (assoc agent
                      :primary_skillset ["3" "2"]
                      :secondary_skillset ["1"])]

    (consumer/consume! {:new_agent agent*})

    (is (= second-id (str (:id (jobs/get-job! agent*)))))
    (is (= third-id (str (:id (jobs/get-job! agent*)))))
    (is (= first-id (str (:id (jobs/get-job! agent*)))))
    (is (nil? (jobs/get-job! agent*)))))

(deftest should-get-secondary-urgent-last
  (doseq [m jobs] (consumer/consume! m))
  (let [agent*      (assoc agent
                           :primary_skillset   ["1" "3"]
                           :secondary_skillset ["2"])
        urgent-id   "8ab86c18-3fae-4804-bfd9-c3d6e8f66260"
        urgent-task {:new_job
                     {:id     urgent-id
                      :type   "1"
                      :urgent true}}]

    (consumer/consume! {:new_agent agent*})
    (consumer/consume! urgent-task)

    (is (= urgent-id (str (:id (jobs/get-job! agent*)))))
    (is (= first-id (str (:id (jobs/get-job! agent*)))))
    (is (= third-id (str (:id (jobs/get-job! agent*)))))
    (is (= second-id (str (:id (jobs/get-job! agent*)))))
    (is (nil? (jobs/get-job! agent*)))))

(deftest shouldnt-get-anything
  (let [agent* (assoc agent
                      :primary_skillset ["4"]
                      :secondary_skillset [])]

    (consumer/consume! {:new_agent agent*})
    (is (nil? (jobs/get-job! agent*)))
    
    (doseq [m jobs] (consumer/consume! m))
    (is (nil? (jobs/get-job! agent*)))))


